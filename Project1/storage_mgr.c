#include<stdio.h>
#include<stdlib.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<unistd.h>
#include<string.h>
#include<math.h>

#include "storage_mgr.h"


FILE *pageFilePointer;/* manipulating page files */


extern void initStorageManager (void) {
    // Initialize
    printf("We initialize the storage manager here\n");
    pageFilePointer = NULL;
}



extern RC createPageFile (char *fileName) {
    // open file in w+ mode... 'w+' mode creates an empty file for both reading and writing.
    pageFilePointer = fopen(fileName, "w+");

    // Checking if file was successfully opened.
    int check = 0;
    if (pageFilePointer == NULL) {
        check = 1;}
    if (check == 1) {
        return RC_FILE_NOT_FOUND;
    } else {
        // Creating an empty page with size = PAGE_SIZE x char.
        SM_PageHandle emptyPagePointer = (SM_PageHandle) calloc(PAGE_SIZE, sizeof(char));
        // file needs empty page , we write
        int isWriteGood = fwrite(emptyPagePointer, sizeof(char), 4096, pageFilePointer);
        if (isWriteGood >= 4096)
            printf("Good write \n");
        else
            printf("Failed \n");

        // Close file
        fclose(pageFilePointer);

        // let memo go away
        free(emptyPagePointer);

        return RC_OK;
    }
}



extern RC openPageFile (char *fileName, SM_FileHandle *fHandle){
    // Open file in mode r. reading only mode creates an empty file.
    pageFilePointer = fopen(fileName, "r");

    //  Check if the file exits
    if(pageFilePointer != NULL) {
        // update information
        fHandle->fileName = fileName;
        // define initial position
        int initPosition = 0;
        fHandle->curPagePos = initPosition;
        // calculate total size
        struct stat fileInformation; // off_t  st_size  is total size
        // success return 0
        if(fstat(fileno(pageFilePointer), &fileInformation) == -1){
            printf("Failed");
        }

        if (fstat(fileno(pageFilePointer), &fileInformation) == 0){
            fHandle->totalNumPages = fileInformation.st_size/ PAGE_SIZE; //4096
        }
        // undo open file
        fclose(pageFilePointer);
        return RC_OK;
    }else{
        return RC_FILE_NOT_FOUND;
    }
}


extern RC closePageFile (SM_FileHandle *fHandle){
    // close it
    pageFilePointer = NULL;
    return RC_OK;
}



extern RC destroyPageFile (char *fileName){
        // check if delete success return 0
    int isDelete = remove(fileName);
    if (isDelete != 0) {
        printf("remove file failed");
        return 5;
    }
    return RC_OK;
}



/* reading blocks from disc */
extern RC readBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage){
    // Checking if page number is valid
    // page number should greater than 0
    // page number should smaller than total number
    if (pageNum > fHandle->totalNumPages){
        return RC_READ_NON_EXISTING_PAGE;
    }
    if (pageNum < 0){
        return RC_READ_NON_EXISTING_PAGE;
    }

    // Opening file in read mode.
    pageFilePointer = fopen(fHandle->fileName, "r");

    // Checking if file was successfully opened.
    if(pageFilePointer == NULL)
        return RC_FILE_NOT_FOUND;

    // Setting the cursor(pointer) position of the file stream.
    // Position = pageNumber x PAGE_SIZE
    // And the seek is success if fseek() return 0
    int offSetSize = sizeof(char) * pageNum * PAGE_SIZE;

    // use seekStatus to check if the pointer is successfully set

    int seekStatus = fseek(fHandle->mgmtInfo, offSetSize, 0);

    if (seekStatus != 0) {
        printf("seek Failed");
        return 0;
    }


    // set current page to the entered pageNum
    fHandle->curPagePos = pageNum;

    return RC_OK;

}





extern int getBlockPos (SM_FileHandle *fHandle) {
    // get curPagePos
    return fHandle->curPagePos;
}

extern RC readFirstBlock (SM_FileHandle *fHandle, SM_PageHandle memPage) {
    // read 0 store in memPage
    return readBlock(0, fHandle, memPage);
}


extern RC readPreviousBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
    // read previousblock
    // calculate page number
    int currentPageRank = fHandle->curPagePos / PAGE_SIZE;
    int previousePage = currentPageRank - 1;
    return readBlock(previousePage, fHandle, memPage);
}



extern RC readCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
    // calculate page number
    int currentPageRank = fHandle->curPagePos / PAGE_SIZE;
    return readBlock(currentPageRank, fHandle, memPage);
}




extern RC readNextBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
    // calculate page number
    int currentPageRank = fHandle->curPagePos / PAGE_SIZE;
    int nextPage = currentPageRank + 1;
    return readBlock(nextPage, fHandle, memPage);
}




extern RC readLastBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
    // last page
    int lastpage = fHandle->totalNumPages - 1;
    return readBlock(lastpage, fHandle, memPage);
}


/* writing blocks to a page file */
extern RC writeBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage){
    // Checking if page number is valid
    // page number should greater than 0
    // page number should smaller than total number
    if (pageNum > fHandle->totalNumPages){
        return RC_READ_NON_EXISTING_PAGE;
    }
    if (pageNum < 0){
        return RC_READ_NON_EXISTING_PAGE;
    }

    // open file in r+ mode , read and write
    pageFilePointer = fopen(fHandle->fileName, "r+");

    // Checking if file was successfully opened.
    if(pageFilePointer == NULL)
        return RC_FILE_NOT_FOUND;

    // calculate offset and seek the position
    // then the page cannot be written to the file.
    // if faile Return RC_SEEK_FILE_POSITION_ERROR
    int offsetSize = pageNum * PAGE_SIZE * sizeof(char);

    int seekStatus = fseek(fHandle->mgmtInfo, offsetSize, 0); // r

    if (seekStatus != 0) {
        printf("seek failed");
        return 0;
    }

    // If the writing operation does not complete,
    int writtenStatus = fwrite(memPage, sizeof(char), PAGE_SIZE,
                             fHandle->mgmtInfo); // return size

    if (writtenStatus != PAGE_SIZE) {
        return RC_WRITE_FAILED;
    }

    // update information
    fHandle->curPagePos = pageNum;

    return RC_OK;

}



extern RC writeCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
    int w = writeBlock(fHandle->curPagePos, fHandle, memPage);
    return w;
}





extern RC appendEmptyBlock (SM_FileHandle *fHandle){

    // Creating an empty page of size PAGE_SIZE bytes
    SM_PageHandle emptyPage = (SM_PageHandle)calloc(PAGE_SIZE, sizeof(char));

    // Moving the cursor (pointer) position to the beginning of the file stream.
    // And the seek is success if fseek() return 0
    int seekStatus = fseek(pageFilePointer, 0, 2);

    if( seekStatus == 0 ) {
        // Writing an empty page to the file
        fwrite(emptyPage, sizeof(char), PAGE_SIZE, pageFilePointer);
    } else {
        free(emptyPage);
        return RC_WRITE_FAILED;
    }

    free(emptyPage);

    // add one page
    int morePage = fHandle->totalNumPages + 1;
    fHandle->totalNumPages = morePage;
    return RC_OK;
}





extern RC ensureCapacity (int numberOfPages, SM_FileHandle *fHandle){
    if (fHandle->totalNumPages >= numberOfPages) {
        printf("enough page");
        return 1;
    }

    // store totalNumPages to a local variable
    int t;

    for (t = 0; t < (numberOfPages - fHandle->totalNumPages); t++) {
        int appd = appendEmptyBlock(fHandle);
    }
    // 要不要打开
    return RC_OK;
}
