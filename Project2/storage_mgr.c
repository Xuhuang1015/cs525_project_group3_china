#include<stdio.h>
#include<stdlib.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<unistd.h>
#include<string.h>
#include<math.h>

#include "storage_mgr.h"

FILE *pFile;

extern void initStorageManager (void) {
    printf("initialize success");
	pFile = NULL;
}

extern RC createPageFile (char *fileName) {
    // open file in w+ mode
    pFile = fopen(fileName, "w+");
	// check open status
	if(pFile != NULL){
        // need emptyPage
        SM_PageHandle emptyPage = (SM_PageHandle)calloc(PAGE_SIZE, sizeof(char));
        // add empty page to file.
        int writeStatus;
        writeStatus = fwrite(emptyPage, sizeof(char), PAGE_SIZE,pFile);
        if(writeStatus < PAGE_SIZE)
            printf("bad write \n");
        else
            printf("good write \n");
        // Close
        fclose(pFile);
        free(emptyPage);
        return RC_OK;
    } else if (pFile == NULL){
        return 600;
    }
    return RC_OK;
}

extern RC openPageFile (char *fileName, SM_FileHandle *fHandle) {
	// open in r mode
    pFile = fopen(fileName, "r");

	// Checking if file was successfully opened.
	if(pFile != NULL){
        fHandle->fileName = fileName;
        fHandle->curPagePos = 0;
        struct stat fileInfo;
        int fs;
        fs = fstat(fileno(pFile), &fileInfo);
        if(fs >= 0){
            int pageNum = fileInfo.st_size/ PAGE_SIZE;
            fHandle->totalNumPages = pageNum;
            fclose(pFile);
            return RC_OK;
        } else{
            return RC_ERROR;
        }
    } else{
        return RC_FILE_NOT_FOUND;
    }
}

extern RC closePageFile (SM_FileHandle *fHandle) {
    pFile = NULL;
	return RC_OK; 
}


extern RC destroyPageFile (char *fileName) {
	remove(fileName);
	return RC_OK;
}

extern RC readBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage) {
	int totalNum = fHandle->totalNumPages;

    if (pageNum > totalNum){
        return RC_READ_NON_EXISTING_PAGE;
    }
    if(pageNum<0){
        return RC_READ_NON_EXISTING_PAGE;
    }

    pFile = fopen(fHandle->fileName, "r");

    // check error situation
	int seekStatus = fseek(pFile, (pageNum * PAGE_SIZE), SEEK_SET);
	if(seekStatus == 0) {
		if(fread(memPage, sizeof(char), PAGE_SIZE, pFile) < PAGE_SIZE)
			return RC_ERROR;
	} else {
		return RC_READ_NON_EXISTING_PAGE;
	}
	fHandle->curPagePos = ftell(pFile);
	fclose(pFile);
    return RC_OK;
}

extern int getBlockPos (SM_FileHandle *fHandle) {
    return fHandle->curPagePos;
}

extern RC writeBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage) {
	int totalNum = fHandle->totalNumPages;
    if (pageNum > totalNum)
        	return RC_ERROR;
    if(pageNum < 0){
        return RC_ERROR;
    }
    pFile = fopen(fHandle->fileName, "r+");
	int beginP = pageNum * PAGE_SIZE;

	if(pageNum == 0) {
		fseek(pFile, beginP, SEEK_SET);
        int c = 0;
        do {         
            if(feof(pFile))
                appendEmptyBlock(fHandle);
            fputc(memPage[c], pFile);
            c++;
        } while (c<PAGE_SIZE);

		fHandle->curPagePos = ftell(pFile);
		fclose(pFile);
	} else {
		fHandle->curPagePos = beginP;
		fclose(pFile);
		writeCurrentBlock(fHandle, memPage);
	}
	return RC_OK;
}

extern RC writeCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage) {
    pFile = fopen(fHandle->fileName, "r+");
	appendEmptyBlock(fHandle);
    int current = fHandle->curPagePos;
	fseek(pFile, current, SEEK_SET);
	fwrite(memPage, sizeof(char), strlen(memPage), pFile);
	fHandle->curPagePos = ftell(pFile);
	fclose(pFile);
	return RC_OK;
}

extern RC appendEmptyBlock (SM_FileHandle *fHandle) {
	SM_PageHandle emptyBlock = (SM_PageHandle)calloc(PAGE_SIZE, sizeof(char));
    fseek(pFile, 0, SEEK_END);
    fwrite(emptyBlock, sizeof(char), PAGE_SIZE, pFile);
	free(emptyBlock);
	fHandle->totalNumPages++;
	return RC_OK;
}

extern RC ensureCapacity (int numberOfPages, SM_FileHandle *fHandle) {
    pFile = fopen(fHandle->fileName, "a");
	while(numberOfPages > fHandle->totalNumPages)
		appendEmptyBlock(fHandle);
	fclose(pFile);
	return RC_OK;
}
