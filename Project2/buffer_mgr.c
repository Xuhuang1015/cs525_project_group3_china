#include<stdio.h>
#include<stdlib.h>
#include "buffer_mgr.h"
#include "storage_mgr.h"

typedef struct PageFrame
{
	SM_PageHandle data;
	PageNumber pageNum;
	bool isDirty;
	int fixCount;
	int lruNum;
	int recent;
} PageFrame;

int PoolSize = 0;
int totalRead = 0;
int totalWrite = 0;
int stamp = 0;


extern void FIFO(BM_BufferPool *const bm, PageFrame *page)
{
    PageFrame *pFrame = (PageFrame *) bm->mgmtData;

    // find the first page
    int targetFrame;
    targetFrame = totalRead % PoolSize;

    int c = -1;
    do {
        c++;
        if(pFrame[targetFrame].fixCount == 0)
        {
            // check if we can change target page
            if(pFrame[targetFrame].isDirty == TRUE)
            {
                SM_FileHandle fh;
                openPageFile(bm->pageFile, &fh);
                writeBlock(pFrame[targetFrame].pageNum, &fh, pFrame[targetFrame].data);
                totalWrite++;
            }
            pFrame[targetFrame].data = page->data;
            pFrame[targetFrame].pageNum = page->pageNum;
            pFrame[targetFrame].isDirty = page->isDirty;
            pFrame[targetFrame].fixCount = page->fixCount;
            break;
        }
        else
        {
            targetFrame++;
            targetFrame = (targetFrame % PoolSize == 0) ? 0 : targetFrame;
        }
    } while (c < PoolSize);
}

extern void LRU(BM_BufferPool *const bm, PageFrame *page)
{	
	PageFrame *pFrame = (PageFrame *) bm->mgmtData;
	int lruIndex, lruNum;

	// go through all pages
    int d = -1;
    do {
        d++;
        if(pFrame[d].fixCount == 0)
        {
            lruIndex = d;
            lruNum = pFrame[d].lruNum;
            break;
        }
    } while (d < PoolSize);
	for(int i = lruIndex + 1; i < PoolSize; i++)
	{
		if(pFrame[i].lruNum < lruNum)
		{
            lruIndex = i;
            lruNum = pFrame[i].lruNum;
		}
	}

	// check dirty flag
	if(pFrame[lruIndex].isDirty == true)
	{
		SM_FileHandle fHandle;
		openPageFile(bm->pageFile, &fHandle);
		writeBlock(pFrame[lruIndex].pageNum, &fHandle, pFrame[lruIndex].data);
        totalWrite++;
	}
    pFrame[lruIndex].data = page->data;
    pFrame[lruIndex].pageNum = page->pageNum;
    pFrame[lruIndex].isDirty = page->isDirty;
    pFrame[lruIndex].fixCount = page->fixCount;
    pFrame[lruIndex].lruNum = page->lruNum;
}

extern RC initBufferPool(BM_BufferPool *const bm, const char *const pageFileName, 
		  const int numPages, ReplacementStrategy strategy, 
		  void *stratData)
{
	bm->pageFile = (char *)pageFileName;
	bm->numPages = numPages;
	bm->strategy = strategy;
	PageFrame *pagePointer = malloc(sizeof(PageFrame) * numPages);

    PoolSize = numPages;

	int d = 0;
    while (d < PoolSize){
        pagePointer[d].data = NULL;
        pagePointer[d].pageNum = -1;
        pagePointer[d].isDirty = false;
        pagePointer[d].fixCount = 0;
        pagePointer[d].lruNum = 0;
        d++;
    }


	bm->mgmtData = pagePointer;
    totalWrite = 0;
	return RC_OK;
		
}

extern RC shutdownBufferPool(BM_BufferPool *const bm)
{
    PageFrame *pageFrame = (PageFrame *)bm->mgmtData;
    forceFlushPool(bm);
    free(pageFrame);
    bm->mgmtData = NULL;
    return RC_OK;
}

extern RC forceFlushPool(BM_BufferPool *const bm)
{
    PageFrame *pageFrame = (PageFrame *)bm->mgmtData;

    int c = 0;
    do {
        if(pageFrame[c].isDirty == true)
        {
            SM_FileHandle fileH;
            openPageFile(bm->pageFile, &fileH);
            // write back to disk
            writeBlock(pageFrame[c].pageNum, &fileH, pageFrame[c].data);
            pageFrame[c].isDirty = false;
            // update counting
            totalWrite++;
        }
        c++;
    } while (c < PoolSize);
    return RC_OK;
}


extern RC markDirty (BM_BufferPool *const bm, BM_PageHandle *const page)
{
    PageFrame *pFrame = (PageFrame *)bm->mgmtData;

    // check all pages in buffer
    int c = -1;
    do {
        c += 1;
        if(pFrame[c].pageNum == page->pageNum)
        {
            pFrame[c].isDirty = true;
            return RC_OK;
        }
    } while (c < PoolSize);

    return RC_OK;
}

extern RC unpinPage (BM_BufferPool *const bm, BM_PageHandle *const page)
{
    PageFrame *pFrame = (PageFrame *)bm->mgmtData;

    // find target page and change
    int c = -1;
    do {
        c += 1;
        if(pFrame[c].pageNum == page->pageNum)
        {
            pFrame[c].fixCount--;
            break;
        }
    } while (c < PoolSize);

    return RC_OK;
}

extern RC forcePage (BM_BufferPool *const bm, BM_PageHandle *const page)
{
    PageFrame *pFrame = (PageFrame *)bm->mgmtData;
    int c = 0;
    do {
        // find matched page
        if(pFrame[c].pageNum == page->pageNum)
        {
            SM_FileHandle fileh;
            openPageFile(bm->pageFile, &fileh);
            writeBlock(pFrame[c].pageNum, &fileh, pFrame[c].data);
            // change dirty
            pFrame[c].isDirty = FALSE;
            // one more wirte
            totalWrite++;
        }
        c += 1;
    } while (c < PoolSize);
    return RC_OK;
}

extern RC pinPage (BM_BufferPool *const bm, BM_PageHandle *const page,
	    const PageNumber pageNum)
{
	PageFrame *pFrame = (PageFrame *)bm->mgmtData;
	if(pFrame[0].pageNum == -1)
	{
		SM_FileHandle fHandle;
		openPageFile(bm->pageFile, &fHandle);
		pFrame[0].data = (SM_PageHandle) malloc(PAGE_SIZE);
		ensureCapacity(pageNum,&fHandle);
		readBlock(pageNum, &fHandle, pFrame[0].data);
        pFrame[0].lruNum = stamp;
        page->pageNum = pageNum;
        page->data = pFrame[0].data;
		pFrame[0].pageNum = pageNum;
		pFrame[0].fixCount++;
        totalRead = stamp = 0;
		return RC_OK;
	}
	else
	{
		bool isFull = true;
        int c = 0;
        do {
            
            if(pFrame[c].pageNum == -1){
                SM_FileHandle fileh;
                openPageFile(bm->pageFile, &fileh);
                pFrame[c].data = (SM_PageHandle) malloc(PAGE_SIZE);
                readBlock(pageNum, &fileh, pFrame[c].data);
                pFrame[c].pageNum = pageNum;
                pFrame[c].fixCount = 1;
                totalRead++;
                stamp++;
                if(bm->strategy == RS_LRU)
                    pFrame[c].lruNum = stamp;
                else if(bm->strategy == RS_CLOCK)
                    pFrame[c].lruNum = 1;
                page->pageNum = pageNum;
                page->data = pFrame[c].data;
                isFull = false;
                break;
            } else{
                if(pFrame[c].pageNum == pageNum)
                {
                    pFrame[c].fixCount++;
                    isFull = false;
                    stamp++;
                    if(bm->strategy == RS_LRU)
                        pFrame[c].lruNum = stamp;
                    page->pageNum = pageNum;
                    page->data = pFrame[c].data;
                    break;
                }
            }
            c += 1;
        } while (c < PoolSize);
		if(isFull == true)
		{
			PageFrame *npFrame = (PageFrame *) malloc(sizeof(PageFrame));
			SM_FileHandle fileh;
			openPageFile(bm->pageFile, &fileh);
            totalRead++;
            stamp++;
            npFrame->data = (SM_PageHandle) malloc(PAGE_SIZE);
			readBlock(pageNum, &fileh, npFrame->data);
            npFrame->fixCount = 1;
            npFrame->pageNum = pageNum;
            npFrame->isDirty = false;
			if(bm->strategy == RS_LRU)
                npFrame->lruNum = stamp;
			else {
                npFrame->lruNum = 1;
            }
			page->pageNum = pageNum;
			page->data = npFrame->data;

            if (bm->strategy == RS_FIFO){
                FIFO(bm, npFrame);
            } else if (bm->strategy == RS_LRU){
                LRU(bm, npFrame);
            } else{
                printf("not ready yet");
            }

		}
		return RC_OK;
	}
}

extern PageNumber *getFrameContents (BM_BufferPool *const bm)
{
	PageNumber *frameContents = malloc(sizeof(PageNumber) * PoolSize);
	PageFrame *pageFrame = (PageFrame *) bm->mgmtData;
    for (int j = 0; j < PoolSize; ++j) {
        if(pageFrame[j].pageNum == -1){
            frameContents[j] = NO_PAGE;
        } else{
            frameContents[j] = pageFrame[j].pageNum;
        }
    }
	return frameContents;
}

extern bool *getDirtyFlags (BM_BufferPool *const bm)
{
    bool *dirtyFlags = malloc(sizeof(bool) * PoolSize);
    PageFrame *pageFrame = (PageFrame *)bm->mgmtData;

    int i = 0;
    do {
   
        dirtyFlags[i] = pageFrame[i].isDirty;
        i += 1;
    } while (i < PoolSize);

    return dirtyFlags;
}


extern int *getFixCounts (BM_BufferPool *const bm)
{
    int *fixCounts = malloc(sizeof(int) * PoolSize);
    PageFrame *pFrame= (PageFrame *)bm->mgmtData;

    for (int i = 0; i < PoolSize; ++i) {
        fixCounts[i] = pFrame[i].fixCount;
    }
    return fixCounts;
}


extern int getNumReadIO (BM_BufferPool *const bm)
{
    return (totalRead + 1);
}

extern int getNumWriteIO (BM_BufferPool *const bm)
{
    return totalWrite;
}
