#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "record_mgr.h"
#include "buffer_mgr.h"
#include "storage_mgr.h"


typedef struct RecordManager
{
	BM_PageHandle pageHandle;
	BM_BufferPool bufferPool;
	RID recordID;
	Expr *condi;
	int tuplesNum, freePage, scanNum;
} RecordManager;

const int arrtriSize = 15;
const int offSet = sizeof(int);

RecordManager *recordPointer;



// find space in table
int findSpace(char *data, int recordSize)
{
	int space = PAGE_SIZE / recordSize;
    int indicator = -1;

    int d = 0;
    do {
        if(data[d * recordSize] != '+'){
            return d;
        }
        d++;
    } while (d < space);
	return indicator;
}


extern RC initRecordManager (void *mgmtData)
{
    printf("Initialized here \n");
	initStorageManager();
	return RC_OK;
}

extern RC shutdownRecordManager ()
{
    recordPointer = NULL;
	free(recordPointer);
	return RC_OK;
}

// create table by schema
extern RC createTable (char *name, Schema *schema)
{
	// open memo
    recordPointer = (RecordManager*) malloc(sizeof(RecordManager));

	// Init buffer pool
	initBufferPool(&recordPointer->bufferPool, name, 100, RS_LRU, NULL);

	char data[PAGE_SIZE];
	char *pHandle = data;

    // first page
    for (int i = 0; i < 2; ++i) {
        *(int*)pHandle = i;
        pHandle += sizeof(int);
    }


	// setting schema
	*(int*)pHandle = schema->numAttr;
	pHandle += sizeof(int);
	*(int*)pHandle = schema->keySize;
	pHandle += sizeof(int);
	
	int d = 0;
    do {
        strncpy(pHandle, schema->attrNames[d], arrtriSize);
        pHandle += arrtriSize;

        // set datatype
        *(int*)pHandle = (int)schema->dataTypes[d];
        pHandle += sizeof(int);

        // set length
        *(int*)pHandle = (int) schema->typeLength[d];
        pHandle += sizeof(int);
        d++;
    } while (d < schema->numAttr);


	SM_FileHandle fHandle;

    createPageFile(name);
    openPageFile(name, &fHandle);
    writeBlock(0, &fHandle, data);
    closePageFile(&fHandle);


	return RC_OK;
}

// opens the table
extern RC openTable (RM_TableData *rel, char *name)
{
	SM_PageHandle pHandle;

    Schema *schema;
    schema = (Schema*) malloc(sizeof(Schema));

	// Set table data
	rel->mgmtData = recordPointer;
	rel->name = name;
    
	// load page into buffer
	pinPage(&recordPointer->bufferPool, &recordPointer->pageHandle, 0);
	// set pointer
	pHandle = (char*) recordPointer->pageHandle.data;
    // get # of tuples
    recordPointer->tuplesNum= *(int*)pHandle;
	pHandle += offSet;
	// get free page
    recordPointer->freePage= *(int*) pHandle;
    pHandle += offSet;
	// get attri
    int attriNumb = *(int*)pHandle;
	pHandle += offSet;
	// Allocating memory space to 'schema'

    
	// Set schema
	schema->numAttr = attriNumb;
	schema->attrNames = (char**) malloc(sizeof(char*) *attriNumb);
	schema->dataTypes = (DataType*) malloc(sizeof(DataType) *attriNumb);
	schema->typeLength = (int*) malloc(sizeof(int) *attriNumb);

	// Allocate memory space for storing attribute name for each attribute
	int d = 0;
    do {
        schema->attrNames[d]= (char*) malloc(arrtriSize);
        d++;
    } while (d < attriNumb);

    int j = 0;
    do {
        // Setting attribute name
        strncpy(schema->attrNames[j], pHandle, arrtriSize);
        pHandle += arrtriSize;

        // set data
        schema->dataTypes[j]= *(int*) pHandle;
        pHandle += offSet;

        // Setting length of datatype (length of STRING) of the attribute
        schema->typeLength[j]= *(int*)pHandle;
        pHandle += offSet;
        j++;
    } while (j < schema->numAttr);
      

	
	// set schema
	rel->schema = schema;	

	// unpin page
	unpinPage(&recordPointer->bufferPool, &recordPointer->pageHandle);

	// write back
	forcePage(&recordPointer->bufferPool, &recordPointer->pageHandle);

	return RC_OK;
}   
  

extern RC closeTable (RM_TableData *rel)
{
	RecordManager *recordPointer = rel->mgmtData;
	shutdownBufferPool(&recordPointer->bufferPool);
	return RC_OK;
}


extern RC deleteTable (char *name)
{
	destroyPageFile(name);
	return RC_OK;
}


extern int getNumTuples (RM_TableData *rel)
{
	RecordManager *recordPointer = rel->mgmtData;
	return recordPointer->tuplesNum;
}




// insert into rel
extern RC insertRecord (RM_TableData *rel, Record *record)
{
	// open pointer
	RecordManager *recordPointer = rel->mgmtData;
	// Set rid
	RID *rID = &record->id;
	char *rdData;
	// get record size
	int rdSize = getRecordSize(rel->schema);
	// Set first page
    rID->page = recordPointer->freePage;
	// load in page
	pinPage(&recordPointer->bufferPool, &recordPointer->pageHandle, rID->page);
	// set data
    rdData = recordPointer->pageHandle.data;
	// find space
    rID->slot = findSpace(rdData, rdSize);
    loop: if (rID->slot == -1){
        unpinPage(&recordPointer->bufferPool, &recordPointer->pageHandle);
        rID->page++;
        pinPage(&recordPointer->bufferPool, &recordPointer->pageHandle, rID->page);
        rdData = recordPointer->pageHandle.data;
        rID->slot = findSpace(rdData, rdSize);
        goto loop;
    }
    char *sPointer;
	sPointer = rdData;
	int position = rID->slot * rdSize;
	// move to position
    sPointer += position;
	*sPointer = '+';
	// copy data
	memcpy(++sPointer, record->data + 1, rdSize - 1);
	// remove page
	unpinPage(&recordPointer->bufferPool, &recordPointer->pageHandle);
	// Incrementing count of tuples
    recordPointer->tuplesNum++;
	// load the page back
	pinPage(&recordPointer->bufferPool, &recordPointer->pageHandle, 0);
	return RC_OK;
}

// delete function
extern RC deleteRecord (RM_TableData *rel, RID id)
{
    RecordManager *recordPointer = rel->mgmtData;
	// load target page
	pinPage(&recordPointer->bufferPool, &recordPointer->pageHandle, id.page);
	// new free page
    recordPointer->freePage = id.page;
	char *data = recordPointer->pageHandle.data;
	// calculate size
	int rdSize = getRecordSize(rel->schema);
	// update data pointer
	data += (id.slot * rdSize);
	// mark the data
    char marker = '-';
	*data = marker;
	return RC_OK;
}

// update on rel
extern RC updateRecord (RM_TableData *rel, Record *record)
{
	RecordManager *recordPointer = rel->mgmtData;
    // load page
	pinPage(&recordPointer->bufferPool, &recordPointer->pageHandle, record->id.page);
	char *data;
	// get size
	int rdSize = getRecordSize(rel->schema);
	// Set the Record's ID
	RID id = record->id;
	// get data and position
	data = recordPointer->pageHandle.data;
    int startPoint = id.slot * rdSize;
	data += startPoint;
	// mark the data
	*data = '+';
	// copy to new record
    int newSize = rdSize - 1;
	memcpy(++data, record->data + 1, newSize);
	// remove page
	unpinPage(&recordPointer->bufferPool, &recordPointer->pageHandle);
	return RC_OK;	
}

// read record
extern RC getRecord (RM_TableData *rel, RID id, Record *record)
{
	RecordManager *recordPointer = rel->mgmtData;
    // load page
	pinPage(&recordPointer->bufferPool, &recordPointer->pageHandle, id.page);

	// calculate size
	int rdSize = getRecordSize(rel->schema);
	char *dPointer = recordPointer->pageHandle.data;
	int newPosition = id.slot * rdSize;
    dPointer += newPosition;

	if(*dPointer == '+')
	{
        record->id = id;
        char *data = record->data;
        dPointer += 1;
        rdSize -= 1;
        memcpy(++data, dPointer, rdSize);
	} else{
        // error case
        return -99;
    }
    // remove page
	unpinPage(&recordPointer->bufferPool, &recordPointer->pageHandle);

	return RC_OK;
}




// scan record
extern RC startScan (RM_TableData *rel, RM_ScanHandle *scan, Expr *cond)
{
	// Open table
	openTable(rel, "ScanTable");
    RecordManager *scanM;
	RecordManager *tableM;
	// open space
    scanM = (RecordManager*) malloc(sizeof(RecordManager));
    scan->mgmtData = scanM;
	// start
    scanM->recordID.page = 1;
	scanM->recordID.slot = 0;
	// initialized
	scanM->scanNum = 0;
	// condition
    scanM->condi = cond;
	// set table
    tableM = rel->mgmtData;
	// Setting the tuple count
    tableM->tuplesNum = arrtriSize;
    scan->rel= rel;
	return RC_OK;
}

// next tuple
extern RC next (RM_ScanHandle *scan, Record *record)
{
	// initialize
	RecordManager *scanM = scan->mgmtData;
	RecordManager *tableM = scan->rel->mgmtData;
    Schema *schema = scan->rel->schema;
	Value *result = (Value *) malloc(sizeof(Value));
	char *data;
	// record size
	int rdSize = getRecordSize(schema);
	// how many scan
	int scanNumb = scanM->scanNum;
	// how many tuples
	int tupleNumb = tableM->tuplesNum;


    loop: if(scanNumb <= tupleNumb){
        if (scanNumb > 0)
        {
            scanM->recordID.slot++;
            if(scanM->recordID.slot >= PAGE_SIZE / rdSize)
            {
                scanM->recordID.slot = 0;
                scanM->recordID.page++;
            }

        }
        else
        {
            scanM->recordID.page = 1;
            scanM->recordID.slot = 0;
        }
        // load page
        pinPage(&tableM->bufferPool, &scanM->pageHandle, scanM->recordID.page);
        data = scanM->pageHandle.data;
        data += (scanM->recordID.slot * rdSize);

        // update info
        record->id.page = scanM->recordID.page;
        record->id.slot = scanM->recordID.slot;
        char *dataPointer = record->data;

        // mark pointer
        *dataPointer = '-';
        memcpy(++dataPointer, data + 1, rdSize - 1);
        scanM->scanNum++;
        scanNumb++;

        // Test the record
        evalExpr(record, schema, scanM->condi, &result);
        if(result->v.boolV == TRUE)
        {
            unpinPage(&tableM->bufferPool, &scanM->pageHandle);
            return RC_OK;
        }
    goto  loop;
    }
	// remove page
	unpinPage(&tableM->bufferPool, &scanM->pageHandle);
	// reset
	scanM->recordID.page = 1;
	scanM->recordID.slot = 0;
	scanM->scanNum = 0;
	return RC_RM_NO_MORE_TUPLES;
}

// This function closes the scan operation.
extern RC closeScan (RM_ScanHandle *scan)
{
    scan->mgmtData = NULL;
    free(scan->mgmtData);
	return RC_OK;
}




// return size
extern int getRecordSize (Schema *schema)
{
	int size = 0;
    int d = 0;
    int attNumb = schema->numAttr;
    loop: if(d < attNumb){
        if(schema->dataTypes[d] == DT_STRING) {
            size += schema->typeLength[d];
        }
        if(schema->dataTypes[d] == DT_INT) {
            size += sizeof(int);
        }
        if(schema->dataTypes[d] == DT_FLOAT) {
            size += sizeof(float);
        }
        if(schema->dataTypes[d] == DT_BOOL) {
            size += sizeof(bool);
        }
        d++;
    goto loop;
    }
	return ++size;
}


// new schema
extern Schema *createSchema (int numAttr, char **attrNames, DataType *dataTypes, int *typeLength, int keySize, int *keys)
{
	Schema *schema = (Schema *) malloc(sizeof(Schema));
	schema->numAttr = numAttr;
	schema->attrNames = attrNames;
	schema->dataTypes = dataTypes;
	schema->typeLength = typeLength;
	schema->keySize = keySize;
	schema->keyAttrs = keys;

	return schema; 
}


extern RC freeSchema (Schema *schema)
{
	free(schema);
	return RC_OK;
}


extern RC createRecord (Record **record, Schema *schema)
{
	Record *nRecord = (Record*) malloc(sizeof(Record));
	// calculate size
	int recordSize = getRecordSize(schema);
	// open space
    nRecord->data= (char*) malloc(recordSize);
	// starting postition
    int start = -1;
    nRecord->id.page = start;
    nRecord->id.slot = start;
	char *dataPointer = nRecord->data;
    // mark empty
	*dataPointer = '-';
    // null
	*(++dataPointer) = '\0';
	// update
	*record = nRecord;
	return RC_OK;
}

// update offset according to schema
RC getOffset (Schema *schema, int attrNum, int *of)
{
	int i = 0;
	*of = 1;

    loop: if(i<attrNum){
        if(schema->dataTypes[i] == DT_STRING){
            *of += schema->typeLength[i];
        }
        if(schema->dataTypes[i] == DT_INT){
            *of += sizeof(int);
        }
        if(schema->dataTypes[i] == DT_FLOAT){
            *of += sizeof(float);
        }
        if(schema->dataTypes[i] == DT_BOOL){
            *of += sizeof(bool);
        }
        i++;
    goto loop;
    }
	return RC_OK;
}

extern RC freeRecord (Record *record)
{
	free(record);
	return RC_OK;
}


extern RC getAttr (Record *record, Schema *schema, int attrNum, Value **value)
{
	int offset = 0;

	// get offset
	getOffset(schema, attrNum, &offset);
	// open space
	Value *attr = (Value*) malloc(sizeof(Value));
	// get pointer
	char *dataP = record->data + offset;
	// If attrNum = 1
	schema->dataTypes[attrNum] = (attrNum == 1) ? 1 : schema->dataTypes[attrNum];

    int dTypes = schema->dataTypes[attrNum];
    if(dTypes == DT_STRING){
        int length = schema->typeLength[attrNum];
        attr->v.stringV = (char *) malloc(length + 1);
        strncpy(attr->v.stringV, dataP, length);
        attr->v.stringV[length] = '\0';
        attr->dt = DT_STRING;
    }
    if(dTypes == DT_INT){
        int value = 0;
        memcpy(&value, dataP, sizeof(int));
        attr->v.intV = value;
        attr->dt = DT_INT;
    }
    if(dTypes == DT_FLOAT){
        float value;
        memcpy(&value, dataP, sizeof(float));
        attr->v.floatV = value;
        attr->dt = DT_FLOAT;
    }
    if(dTypes == DT_BOOL){
        bool value;
        memcpy(&value,dataP, sizeof(bool));
        attr->v.boolV = value;
        attr->dt = DT_BOOL;
    }

	*value = attr;
	return RC_OK;
}


extern RC setAttr (Record *record, Schema *schema, int attrNum, Value *value)
{
	int offset = 0;
	// take offset
	getOffset(schema, attrNum, &offset);
	// Getting the starting position of record's data in memory
	char *dataP = record->data + offset;
	int dType = schema->dataTypes[attrNum];
    if(dType == DT_STRING){

        int length = schema->typeLength[attrNum];

        strncpy(dataP, value->v.stringV, length);
        dataP = dataP + schema->typeLength[attrNum];
    }
    if(dType == DT_INT){
        *(int *) dataP = value->v.intV;
        dataP += sizeof(int);
    }
    if(dType == DT_FLOAT){
        *(float *) dataP = value->v.floatV;
        dataP += sizeof(float);
    }
    if(dType == DT_BOOL){
        *(bool *) dataP = value->v.boolV;
        dataP += sizeof(bool);
    }


	return RC_OK;
}